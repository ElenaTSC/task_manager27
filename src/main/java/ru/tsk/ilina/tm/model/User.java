package ru.tsk.ilina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.util.HashUtil;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {


    private String login;


    private String passwordHash;


    private String email;


    private String firstName;


    private String lastName;


    private String middleName;


    private Role role = Role.USER;

    private boolean locked = false;

}

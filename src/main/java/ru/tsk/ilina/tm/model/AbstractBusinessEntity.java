package ru.tsk.ilina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsk.ilina.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractOwnerEntity {

    private String name = "";
    private String description = "";
    private Status status = Status.NOT_STARTED;
    private Date startDate;
    private Date finishDate;
    private Date createdDate = new Date();

    @Override
    public String toString() {
        return id + " : " + name;
    }

}

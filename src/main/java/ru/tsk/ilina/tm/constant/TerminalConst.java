package ru.tsk.ilina.tm.constant;

import org.jetbrains.annotations.NotNull;

public class TerminalConst {

    @NotNull
    public static final String ABOUT = "about";
    @NotNull
    public static final String HELP = "help";
    @NotNull
    public static final String VERSION = "version";
    @NotNull
    public static final String EXIT = "exit";
    @NotNull
    public static final String INFO = "info";
    @NotNull
    public static final String COMMANDS = "commands";
    @NotNull
    public static final String ARGUMENTS = "arguments";
    @NotNull
    public static final String TASK_CREATE = "task-create";
    @NotNull
    public static final String TASK_CLEAR = "task-clear";
    @NotNull
    public static final String TASK_LIST = "task-list";
    @NotNull
    public static final String TASK_SHOW_BY_ID = "task-show-by-id";
    @NotNull
    public static final String TASK_SHOW_BY_INDEX = "task-show-by-index";
    @NotNull
    public static final String TASK_SHOW_BY_NAME = "task-show-by-name";
    @NotNull
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    @NotNull
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    @NotNull
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    @NotNull
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";
    @NotNull
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    @NotNull
    public static final String TASK_START_BY_ID = "task-start-by-id";
    @NotNull
    public static final String TASK_START_BY_INDEX = "task-start-by-index";
    @NotNull
    public static final String TASK_START_BY_NAME = "task-start-by-name";
    @NotNull
    public static final String TASK_FINISH_BY_ID = "task-finish-by-id";
    @NotNull
    public static final String TASK_FINISH_BY_INDEX = "task-finish-by-index";
    @NotNull
    public static final String TASK_FINISH_BY_NAME = "task-finish-by-name";
    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_ID = "task_change_status_by_id";
    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_INDEX = "task_change_status_by_index";
    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_NAME = "task_change_status_by_name";
    @NotNull
    public static final String PROJECT_CREATE = "project-create";
    @NotNull
    public static final String PROJECT_CLEAR = "project-clear";
    @NotNull
    public static final String PROJECT_LIST = "project-list";
    @NotNull
    public static final String PROJECT_SHOW_BY_ID = "project-show-by-id";
    @NotNull
    public static final String PROJECT_SHOW_BY_INDEX = "project-show-by-index";
    @NotNull
    public static final String PROJECT_SHOW_BY_NAME = "project-show-by-name";
    @NotNull
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    @NotNull
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    @NotNull
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    @NotNull
    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";
    @NotNull
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    @NotNull
    public static final String PROJECT_START_BY_ID = "project-start-by-id";
    @NotNull
    public static final String PROJECT_START_BY_INDEX = "project-start-by-index";
    @NotNull
    public static final String PROJECT_START_BY_NAME = "project-start-by-name";
    @NotNull
    public static final String PROJECT_FINISH_BY_ID = "project-finish-by-id";
    @NotNull
    public static final String PROJECT_FINISH_BY_INDEX = "project-finish-by-index";
    @NotNull
    public static final String PROJECT_FINISH_BY_NAME = "project-finish-by-name";
    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_ID = "project_change_status_by_id";
    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_INDEX = "project_change_status_by_index";
    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_NAME = "project_change_status_by_name";
    @NotNull
    public static final String PROJECT_TASK_BIND = "project_bind_task_by_id";
    @NotNull
    public static final String PROJECT_TASK_UNBIND = "project_unbind_task_by_id";
    @NotNull
    public static final String PROJECT_TASK_REMOVE_BY_ID = "project_remove_task_by_id";
    @NotNull
    public static final String PROJECT_TASK_FIND_BY_ID = "project_task_find_by_id";

}

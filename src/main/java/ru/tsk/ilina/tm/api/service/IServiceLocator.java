package ru.tsk.ilina.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull ITaskService getTaskService();

    @NotNull IProjectService getProjectService();

    @NotNull IProjectTaskService getProjectTaskService();

    @NotNull ICommandService getCommandService();

    @NotNull IAuthService getAuthService();

    @NotNull IUserService getUserService();

    @NotNull IPropertyService getPropertyService();

}

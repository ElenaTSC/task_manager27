package ru.tsk.ilina.tm.api.repository;


import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IAbstractBusinessRepository<Task> {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Task bindTaskToProjectById(String userId, String projectId, String taskId);

    Task unbindTaskToProjectById(String userId, String projectId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

}

package ru.tsk.ilina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractTaskCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findByID(userId, id);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskService().removeByID(userId, id);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}

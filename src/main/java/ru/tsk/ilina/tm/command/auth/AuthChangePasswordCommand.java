package ru.tsk.ilina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.service.IUserService;
import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class AuthChangePasswordCommand extends AbstractAuthUserCommand {

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change password of the current user";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("[ENTER NEW PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().setPassword(userId, password);
        System.out.println("[OK]");
    }

}

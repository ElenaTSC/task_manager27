package ru.tsk.ilina.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id is empty");
    }

    public EmptyIdException(@NotNull final String message) {
        super("Error! " + message + " id is empty");
    }

}

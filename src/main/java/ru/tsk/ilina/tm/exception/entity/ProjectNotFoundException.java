package ru.tsk.ilina.tm.exception.entity;

import ru.tsk.ilina.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found");
    }

}

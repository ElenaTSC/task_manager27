package ru.tsk.ilina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.repository.ITaskRepository;
import ru.tsk.ilina.tm.api.service.ITaskService;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.empty.*;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractBusinessService<Task, ITaskRepository> implements ITaskService {

    public TaskService(ITaskRepository repository) {
        super(repository);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String name, @Nullable String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        repository.add(userId, task);
    }

}

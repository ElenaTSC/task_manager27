package ru.tsk.ilina.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.api.repository.IAbstractRepository;
import ru.tsk.ilina.tm.api.service.IAbstractService;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyNameException;
import ru.tsk.ilina.tm.exception.empty.EmptyUserIdException;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.AbstractEntity;
import ru.tsk.ilina.tm.repository.AbstractRepository;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity, R extends IAbstractRepository<E>> implements IAbstractService<E> {

    protected final R repository;

    protected AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E add(@NotNull final E entity) {
        return repository.add(entity);
    }

    @Override
    public void addAll(@NotNull List<E> entities) {
        repository.addAll(entities);
    }

    @Override
    public E findById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public E remove(@NotNull final E entity) {
        return repository.remove(entity);
    }

}

package ru.tsk.ilina.tm.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.model.User;

import java.io.Serializable;
import java.util.List;

public class Domain implements Serializable {

    @NotNull
    private List<User> users;
    @NotNull
    private List<Project> projects;
    @NotNull
    private List<Task> tasks;

    public void setUsers(@NotNull List<User> users) {
        this.users = users;
    }

    public void setProjects(@NotNull List<Project> projects) {
        this.projects = projects;
    }

    public void setTasks(@NotNull List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }
}
